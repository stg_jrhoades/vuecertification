// Mock Guards
import {createLocalVue, mount} from "@vue/test-utils";
import { routes } from "@/router";
import VueRouter from "vue-router";
import App from "../../src/App";
import Landing from "../../src/views/Landing";
import NotFound from "../../src/views/NotFound";
import Search from "../../src/views/Search";
import Favorites from "../../src/views/Favorites";

// Mock Components
jest.mock("@/views/Landing.vue", () => ({ name: "Landing", render: h => h("div") }));
jest.mock("@/views/Search.vue", () => ({ name: "Search", render: h => h("div") }));
jest.mock("@/views/Favorites.vue", () => ({ name: "Favorites", render: h => h("div") }));
jest.mock("@/views/NotFound.vue", () => ({ name: "NotFound", render: h => h("div") }));

const localVue = createLocalVue()
localVue.use(VueRouter)

describe("router", () => {
    let router;
    let wrapper;

    beforeEach(() => {
        router = new VueRouter({routes})
        wrapper = mount(App, {
            localVue,
            router,
        })
    })

    it("can load page", async () => {
        router.push("/").catch(() => {});
        await wrapper.vm.$nextTick();
        expect(wrapper.findComponent(Landing).exists()).toBeTruthy()
    });

    it("can load not found page", async () => {
        router.push("/bob").catch(() => {});
        await wrapper.vm.$nextTick();
        expect(wrapper.findComponent(NotFound).exists()).toBeTruthy()
    })

    it("can load search page", async () => {
        router.push("/search").catch(() => {});
        await wrapper.vm.$nextTick();
        expect(wrapper.findComponent(Search).exists()).toBeFalsy()
    })

    it("can load favorites page", async () => {
        router.push("/favorites").catch(() => {});
        await wrapper.vm.$nextTick();
        expect(wrapper.findComponent(Favorites).exists()).toBeFalsy()
    })
});