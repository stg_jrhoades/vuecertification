import axios from "axios";
import {convert_time, getVideoDetails, ytSearch} from "../../../src/services/youtube";

jest.mock("axios", () => ({
    get: jest.fn()
}));

describe("youtube service", () => {
    it("should return mocked search data", async () => {
        axios.get.mockImplementation(() => Promise.resolve({ data: { items: [1, 2, 3] } }));
        const resp = await ytSearch("something", "someKey");
        expect(resp.length).toBe([1,2,3].length);
    });

    it("should return mocked details", async () => {
        axios.get.mockImplementation(() => Promise.resolve({ data: { message: "mocked" } }));
        const resp = await getVideoDetails("someId", "someKey");
        expect(resp.data.message).toBe("mocked");
    });

    it("should calculate timestamp to seconds", () => {
        expect(convert_time("PT1H10M30S")).toBe(4230);
        expect(convert_time("PT10H")).toBe(36000);
        expect(convert_time("PT30M")).toBe(1800);
        expect(convert_time("PT50S")).toBe(50);
    })
})