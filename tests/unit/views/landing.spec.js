import {createLocalVue, mount} from "@vue/test-utils";
import Landing from "../../../src/views/Landing";
import Vuex from "vuex";

import {library} from "@fortawesome/fontawesome-svg-core";
import { faMask } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";


const localVue = createLocalVue()
localVue.use(Vuex);

library.add(faMask)
localVue.component('fa-icon', FontAwesomeIcon)

const $route = {
    path: "search"
}

const $router = {
    push: jest.fn()
}

describe("landing", () => {
    let wrapper;
    let store;
    let state;
    let actions;

    beforeEach(() => {
        actions = {
            login: jest.fn(() => Promise.resolve()),
        }
        state = {
            isAuthenticated: false
        }

        store = new Vuex.Store({
            state,
            actions
        })

        jest.useFakeTimers();

        wrapper = mount(Landing, {
            stubs: ["router-link"],
            mocks: {
                $route,
                $router
            },
            localVue,
            store
        });
    })


    it("should render landing", () => {
        expect(wrapper.findComponent(Landing).exists()).toBeTruthy()
    })

    it("should attempt to login", async () => {
        const username = wrapper.find("input#email")
        username.setValue("testing");
        expect(wrapper.find("input#email").element.value).toBe("testing");

        const password = wrapper.find("input#password");
        password.setValue("Testing01");
        expect(wrapper.find("input#password").element.value).toBe("Testing01");

        await wrapper.vm.$nextTick();

        const login = wrapper.find("button[type='submit']");
        await login.trigger("click");

        await wrapper.vm.$nextTick();
        jest.runAllTimers();
        expect(actions.login).toHaveBeenCalled();
        expect($router.push).toHaveBeenCalled();
    })

    it("should change password type", () => {
        wrapper.find("span#mask").trigger("click");
        expect(wrapper.find("input[type='text']").exists()).toBeTruthy();
    })
})