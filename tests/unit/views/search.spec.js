import Vuex from "vuex";
import {createLocalVue, mount} from "@vue/test-utils";
import Search from "../../../src/views/Search";
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import {getVideoDetails, ytSearch} from "../../../src/services/youtube";


jest.mock("@/services/youtube.js", () => ({
    getVideoDetails: jest.fn(() => Promise.resolve({
        data: {
            items: [
                {
                    id: 10,
                    snippet: {
                        thumbnails: {
                            default: {
                                url: "/"
                            }
                        },
                        title: "Test 1",
                        description: "testing"
                    }
                }
            ]
        }
    })),
    ytSearch: jest.fn(() => Promise.resolve([
        {
            id: {
                videoId: 10
            },
            snippet: {
                thumbnails: {
                    default: {
                        url: "/"
                    }
                },
                title: "Test 1"
            }
        },
        {
            id: {
                videoId: 11
            },
            snippet: {
                thumbnails: {
                    default: {
                        url: "/"
                    }
                },
                title: "Test 2"
            }
        },
    ]))
}))

const localVue = createLocalVue();
localVue.use(Vuex);

library.add(faStar)
localVue.component('fa-icon', FontAwesomeIcon)


describe("search", () => {
    let wrapper;
    let state;
    let actions;
    let store;

    beforeEach(() => {
        actions = {
            addFavorite: jest.fn(),
            removeFavorite: jest.fn()
        };
        state = {
            apiKey: "",
            favorites: [
                {
                    id: 11,
                    snippet: {
                        thumbnails: {
                            default: {
                                url: "/"
                            }
                        },
                        title: "Test 2",
                        description: "testing"
                    }
                }
            ]
        };

        store = new Vuex.Store({
            state,
            actions
        })

        wrapper = mount(Search, {
            localVue,
            store
        })
    })


    it("should look up attempt to look up videos", async () => {
        wrapper.find("input#search-input").setValue("test");
        expect(wrapper.find("input#search-input").element.value).toBe("test");

        wrapper.find("button#search-button").trigger("click");
        await wrapper.vm.$nextTick();

        expect(ytSearch).toHaveBeenCalled();
        expect(getVideoDetails).toBeCalled();
    })
    it("should add and remove from favorites", async () => {
        wrapper.find("input#search-input").setValue("test");
        expect(wrapper.find("input#search-input").element.value).toBe("test");

        wrapper.find("button#search-button").trigger("click");
        await wrapper.vm.$nextTick();

        // Second tick is for the v-for to update dom
        await wrapper.vm.$nextTick();

        // Tests adding favorite
        wrapper.find("div.favorite").trigger("click");
        await wrapper.vm.$nextTick();

        expect(actions.addFavorite).toHaveBeenCalled();

        // Test removing favorite
        wrapper.find("div.favorited").trigger("click");
        await wrapper.vm.$nextTick();

        expect(actions.removeFavorite).toHaveBeenCalled();
    })
})