import {createLocalVue, mount} from "@vue/test-utils";
import Vuex from "vuex";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faStar} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import Favorites from "../../../src/views/Favorites";

const localVue = createLocalVue();
localVue.use(Vuex);

library.add(faStar)
localVue.component('fa-icon', FontAwesomeIcon)

describe("favorites", () => {
    let wrapper;
    let state;
    let store;

    beforeEach(() => {
        state = {
            apiKey: "",
            favorites: [
                {
                    id: 11,
                    snippet: {
                        thumbnails: {
                            default: {
                                url: "/"
                            }
                        },
                        title: "A Test",
                        description: "testing",
                        publishedAt: "2019-05-18T15:30:00Z"
                    },
                    contentDetails: {
                        duration: "PT15M47S"
                    }
                },
                {
                    id: 10,
                    snippet: {
                        thumbnails: {
                            default: {
                                url: "/"
                            }
                        },
                        title: "Test 1",
                        description: "testing",
                        publishedAt: "2019-04-18T15:30:00Z"
                    },
                    contentDetails: {
                        duration: "PT14M47S"
                    }
                },
                {
                    id: 12,
                    snippet: {
                        thumbnails: {
                            default: {
                                url: "/"
                            }
                        },
                        title: "Z Test",
                        description: "testing",
                        publishedAt: "2019-06-18T15:30:00Z"
                    },
                    contentDetails: {
                        duration: "PT16M47S"
                    }
                },
                {
                    id: 13,
                    snippet: {
                        thumbnails: {
                            default: {
                                url: "/"
                            }
                        },
                        title: "X Test",
                        description: "testing",
                        publishedAt: "2019-06-18T15:30:00Z"
                    },
                    contentDetails: {
                        duration: "PT1"
                    }
                },
                {
                    id: 14,
                    snippet: {
                        thumbnails: {
                            default: {
                                url: "/"
                            }
                        },
                        title: "Y Test",
                        description: "testing",
                        publishedAt: "2019-06-18T15:30:00Z"
                    },
                    contentDetails: {
                        duration: "PT1H28M36S"
                    }
                }
            ]
        };

        store = new Vuex.Store({ state });
        wrapper = mount(Favorites, { localVue, store });
    });

    it("should render", () => {
        expect(wrapper.findComponent(Favorites).exists()).toBeTruthy();
    });

    it("should filter by query", () => {
        wrapper.find("input#query-input").setValue("Z");
        wrapper.find("button#query-button").trigger("click");
        wrapper.vm.$nextTick();
        wrapper.vm.$nextTick();

        expect(wrapper.vm.$data.sortedFavorites[0].id).toBe(12);
    })

    it("should NOT filter", () => {
        wrapper.find("input#query-input").setValue("");
        wrapper.find("button#query-button").trigger("click");
        wrapper.vm.$nextTick();
        wrapper.vm.$nextTick();

        expect(wrapper.vm.$data.sortedFavorites[0]).toBeFalsy();
    })

    it("should filter by title", () => {
        wrapper.find("button#title-filter").trigger("click");
        wrapper.vm.$nextTick();
        wrapper.vm.$nextTick();

        expect(wrapper.vm.$data.sortedFavorites[0].id).toBe(11);
    })

    it("should filter by date", () => {
        wrapper.find("button#date-filter").trigger("click");
        wrapper.vm.$nextTick();
        wrapper.vm.$nextTick();

        expect(wrapper.vm.$data.sortedFavorites[0].id).toBe(10);
    })

    it("should filter by length", () => {
        wrapper.find("button#length-filter").trigger("click");
        wrapper.vm.$nextTick();
        wrapper.vm.$nextTick();

        expect(wrapper.vm.$data.sortedFavorites[0].id).toBe(14);
    })

    it("should return to favorite list", () => {
        wrapper.find("button#length-filter").trigger("click");
        wrapper.vm.$nextTick();
        wrapper.vm.$nextTick();

        expect(wrapper.vm.$data.sortedFavorites[0].id).toBe(14);

        wrapper.find("button#length-filter").trigger("click");
        wrapper.vm.$nextTick();
        wrapper.vm.$nextTick();

        expect(wrapper.vm.$data.sortedFavorites[0]).toBeFalsy();
    })
})