import { store } from "@/store";

jest.mock("@/services/youtube.js", () => ({
    getVideoDetails: jest.fn(async (videoId, apiKey) => {
        return {
            data: {
                items: [
                    {
                        id: videoId,
                        key: apiKey
                    }
                ]
            }
        }
    })
}))

describe("store", () => {
    describe("mutations", () => {
        it("should change the state isAuthenticated to truth", () => {
            const state = { isAuthenticated: false }
            store.mutations.updateAuth(state, true);

            expect(state.isAuthenticated).toBeTruthy();
        })

        it("should change the state isAuthenticated to false", () => {
            const state = { isAuthenticated: true }
            store.mutations.updateAuth(state, false);

            expect(state.isAuthenticated).toBeFalsy();
        })

        it("should set new value of favorites", () => {
            const state = { favorites: [] };
            store.mutations.setFavorties(state, [1, 2, 3]);

            expect(state.favorites.length).toBe(3);
        })

        it("should set favorites to empty array", () => {
            const state = { favorites: [1, 2, 3] };
            store.mutations.setFavorties(state, []);

            expect(state.favorites.length).toBe(0);
        })
    })

    describe("actions", () => {
        it("should try run commit statement with setFavorties adding element", async () => {
            const video = { id: 10 };
            const commit = jest.fn();
            const state = { favorites: [] }

            await store.actions.addFavorite({ commit, state }, video);
            expect(commit).toHaveBeenCalledWith("setFavorties", [{"id": undefined, "key": undefined}]);
        });

        it("should try and run commit statement with setFavorites removing element", async () => {
            const video = { id: { videoId: 10 } };
            const commit = jest.fn();
            const state = { favorites: [{ id: { videoId: 10 }}] }

            await store.actions.removeFavorite({ commit, state }, video);
            expect(commit).toHaveBeenCalledWith("setFavorties", []);
        })

        it("should try and run commit statement with updateAuth to true", async () => {
            const commit = jest.fn();

            await store.actions.login({ commit });
            expect(commit).toHaveBeenCalledWith("updateAuth", true);
        })

        it("should try and run commit statement with updateAuth to false", async () => {
            const commit = jest.fn();

            await store.actions.logout({ commit });
            expect(commit).toHaveBeenCalledWith("updateAuth", false);
        })
    })
});