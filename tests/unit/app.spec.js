import { createLocalVue, mount } from '@vue/test-utils'
import App from "@/App.vue"
import VueRouter from "vue-router";

describe("App.vue", () => {
  const localVue = createLocalVue();
  localVue.use(VueRouter);
  const router = new VueRouter();

  it("renders page and header", () => {
    const wrapper = mount(App, {
      localVue,
      router
    });
    expect(wrapper.html()).toContain("<h1>STG Youtube</h1>");
  })
})

