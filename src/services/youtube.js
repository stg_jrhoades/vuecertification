import axios from "axios";

export async function ytSearch(searchTerm, apiKey) {
    const ytUrl = `https://www.googleapis.com/youtube/v3/search?type=video&part=snippet&maxResults=10&order=viewCount&q=${encodeURI(searchTerm)}&key=${apiKey}`;
    const searchResultsResp = await axios.get(ytUrl).catch(err => { console.error(err.toString())});
    return searchResultsResp.data.items;
}

export async function getVideoDetails(videoId, apiKey) {
    const videoUrl = `https://www.googleapis.com/youtube/v3/videos?part=snippet&part=contentDetails&id=${videoId}&key=${apiKey}`;
    return await axios.get(videoUrl).catch(err => { console.error(err.toString()) });
}

export function convert_time(duration) {
    let a = duration.match(/\d+/g);

    if (duration.indexOf('M') >= 0 && duration.indexOf('H') === -1 && duration.indexOf('S') === -1) {
        a = [0, a[0], 0];
    }

    if (duration.indexOf('H') >= 0 && duration.indexOf('M') === -1) {
        a = [a[0], 0, a[1]];
    }
    if (duration.indexOf('H') >= 0 && duration.indexOf('M') === -1 && duration.indexOf('S') === -1) {
        a = [a[0], 0, 0];
    }

    duration = 0;

    if (a.length === 3) {
        duration = duration + parseInt(a[0]) * 3600;
        duration = duration + parseInt(a[1]) * 60;
        duration = duration + parseInt(a[2]);
    }

    if (a.length === 2) {
        duration = duration + parseInt(a[0]) * 60;
        duration = duration + parseInt(a[1]);
    }

    if (a.length === 1) {
        duration = duration + parseInt(a[0]);
    }
    return duration
}