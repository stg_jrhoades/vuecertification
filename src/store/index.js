import Vue from 'vue'
import Vuex from 'vuex'
import {getVideoDetails} from "../services/youtube";

Vue.use(Vuex)

// #Actions, Reducers, and the Store
// the vuex store is amazingly simple and straight forward to work with. Like most things in vue it's just an object where you specify all the values for the state, actions, and mutations you want.
// If your app is large and you need to break this thing down you can make modules which are just stores with namespaces you feed into this store in the module section.
export const store = {
  state: {
    apiKey: "AIzaSyCKn9wqKS5wfd9uLKYnJnRjHEmSsbeNy1M",
    isAuthenticated: false,
    favorites: []
  },
  mutations: {
    updateAuth(state, value) {
      state.isAuthenticated = value
    },
    setFavorties(state, value) {
      state.favorites = value;
    }
  },
  actions: {
    addFavorite(context, video) {
      const data = context.state.favorites;
      getVideoDetails(video.id.videoId, context.state.apiKey).then(resp => {
        data.push(resp.data.items[0]);
        context.commit("setFavorties", data);
      });
    },
    removeFavorite(context, video) {
      let data = context.state.favorites;
      data = data.filter(value => { return value.id.videoId !== video.id.videoId })
      context.commit("setFavorties", data);
    },
    login(context) {
      context.commit("updateAuth", true);
    },
    logout(context) {
      context.commit("updateAuth", false);
    }
  }
}

export default new Vuex.Store(store)
