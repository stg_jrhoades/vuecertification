import Vue from 'vue'
import VueRouter from 'vue-router'
import Landing from "../views/Landing.vue";
import Search from "../views/Search.vue";
import Favorites from "../views/Favorites.vue";
import NotFound from "../views/NotFound";
import store from "../store";

Vue.use(VueRouter)

function requireAuth (to, from, next) {
  // Check the authentication before letting them go next route
  if (!store.state.isAuthenticated) {
    next("/")
    return;
  }

  // Go back to home route
  next();
}

// #Routing
// Routing is pretty straight forward it's just managing an array of objects that help specify all use case you can have. It's ready from top to bottom so your catch all routes should always be at the bottom for consistent results
export const routes = [
  {
    path: '/',
    name: 'Home',
    component: Landing
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
    beforeEnter: requireAuth
  },
  {
    path: "/favorites",
    name: "Favorites",
    component: Favorites,
    beforeEnter: requireAuth
  },
  {
   path: "*",
   name: "NotFound",
   component: NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router