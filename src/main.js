import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import { faStar, faMask } from "@fortawesome/free-solid-svg-icons";

Vue.config.productionTip = false

library.add(faStar)
library.add(faMask)
Vue.component('fa-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
